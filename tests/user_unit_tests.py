import os
from .bases import UnitBaseCase
from flask import render_template, url_for, current_app
from microblog import app, views
from microblog.models import Asset
from pymongo import MongoClient
import gridfs
from urlparse import urlparse
from nose.tools import *
from bson.objectid import ObjectId

class TestUser(UnitBaseCase):
	"""
	Unit tests for the user and profile functionality
	"""

	def setUp(self):
		self.initDB()
		self.initGridFS()
		super(TestUser, self).setUp()
		self.un = "unit_test"
		self.pw = "utpass"
		self.signUp(self.un, self.pw)

	def test_logged_in_user_can_see_user_link(self):
		self.logIn(self.un, self.pw)
		
		res = self.client.get('/index')
		with app.test_request_context():
			href = url_for("user", username=self.un)
			self.assertIn(
				"<a id=\"user_link\" href=\""+href+"\">My Profile</a>",
				res.get_data()
			)

	def test_logged_in_user_can_access_user_page(self):
		self.logIn(self.un, self.pw)
		with app.app_context():
			res = self.client.get(url_for("user", username=self.un))
			self.assertEqual(res.status_code, 200)
			self.assertIn(
				"<title>%s - microblog</title>" % self.un,
				res.get_data()
			)

	def test_anonymous_user_can_not_see_user_link(self):
		res = self.client.get('/index')
		with app.app_context():
			href = url_for("user", username=self.un)
			self.assertNotIn(
				"<a id=\"user_link\" href=\""+href+"\">My Profile</a>",
				res.get_data()
			)

	def test_user_page_shows_default_avatar(self):
		with app.test_request_context():
			res = self.client.get(url_for('user', username=self.un))

			self.assertIn(
				"<img id=\"avatar_top_img\" alt=\"default_avatar.jpg\" "
				"src=\""+url_for('asset', filename="default_avatar.jpg")+"\"/>",
				res.get_data()
			)

	def test_default_avatar(self):
		self.assertTrue(self.grid_fs.exists({"filename":"default_avatar.jpg"}))	

	def test_shows_edit_avatar_form_for_own_user(self):
		self.logIn(self.un, self.pw)
		with app.test_request_context():
			res = self.client.get(url_for('user', username=self.un))

			self.assertIn("<form name=\"avatar_edit_form\"", res.get_data())

	def test_hide_edit_avatar_form_for_anonymous_user(self):
		with app.test_request_context():
			res = self.client.get(url_for('user', username=self.un))
			self.assertNotIn("<form name=\"avatar_edit_form\"", res.get_data())

	def test_hide_edit_avatar_form_for_other_user(self):
		self.signUp("unit_test2", "utpass2")
		self.logIn(self.un, self.pw)
		with app.test_request_context():
			res = self.client.get(url_for('user', username="unit_test2"))

			self.assertNotIn("<form name=\"avatar_edit_form\"", res.get_data())

	def test_show_post_form_for_own_user(self):
		self.logIn(self.un, self.pw)
		with app.test_request_context():
			res = self.client.get(url_for('user', username=self.un))

			self.assertIn("<form name=\"post_form\"", res.get_data()) 

	def test_hide_post_form_for_anonymous_user(self):
		with app.test_request_context():
			res = self.client.get(url_for('user', username=self.un))

			self.assertNotIn("<form name=\"post_form\"", res.get_data())

	def test_hide_post_form_for_other_user(self):
		self.signUp("unit_test2", "utpass2")
		self.logIn(self.un, self.pw)
		with app.test_request_context():
			res = self.client.get(url_for('user', username="unit_test2"))

			self.assertNotIn("<form name=\"post_form\"", res.get_data())

	def test_posts_container_shows_default_message_when_no_posts(self):
		# ensure no posts for this user in the db
		self.clearUserPosts(self.un)
		with app.test_request_context():
			res = self.client.get(url_for('user', username="unit_test"))

			self.assertIn(
				"<p id=\"no_posts_text\">No posts yet!</p>",
				res.get_data()
			)

	def test_shows_default_bio_when_no_bio(self):
		with app.test_request_context():
			res = self.client.get(url_for('user', username="unit_test"))

			self.assertIn(
				"<p id=\"user_bio\">Who are you?</p>",
				res.get_data()
			)

	def test_show_bio_edit_form_for_own_user(self):
		self.logIn(self.un, self.pw)
		with app.test_request_context():
			res = self.client.get(url_for('user', username=self.un))

			self.assertIn("<form name=\"bio_edit_form\"", res.get_data()) 

	def test_hide_bio_edit_form_for_other_user(self):
		self.signUp('unit_test2', 'utpass2')
		self.logIn(self.un, self.pw)
		with app.test_request_context():
			res = self.client.get(url_for('user', username="unit_test2"))

			self.assertNotIn("<form name=\"bio_edit_form\"", res.get_data()) 

	def test_hide_bio_edit_form_for_anonymous_user(self):
		with app.test_request_context():
			res = self.client.get(url_for('user', username=self.un))

			self.assertNotIn("<form name=\"bio_edit_form\"", res.get_data()) 

	def test_logged_in_user_can_update_avatar(self):
		self.logIn(self.un, self.pw)
		with app.test_request_context():
			with app.open_resource(
				os.getcwd()+"/tests/test_avatar.jpg"
				) as file:
				res = self.client.post(url_for('new_asset'), data=dict(
					asset_type="avatar",
					avatar_user=self.un,
					avatar_edit_file=(file,'test_avatar.jpg'),
					avatar_edit_submit='Upload Avatar'
				))
				user = self.db.users.find_one({"username":self.un})
				avatar = self.db.assets.files.find_one({
					"_id":ObjectId(user["avatar_id"])
				})

				self.assertEqual(avatar["filename"], "test_avatar.jpg")

	def test_logged_in_user_can_post(self):
		self.logIn(self.un, self.pw)
		with app.test_request_context():
			res = self.client.post(
				url_for('new_post', username=self.un),
				data=dict(post_field="Test post #1", post_submit="Submit")
			)
			self.assertIn(
				"<p class=\"post_text\">Test post #1</p>",
				res.get_data()
			)

	def test_logged_in_user_can_update_bio(self):
