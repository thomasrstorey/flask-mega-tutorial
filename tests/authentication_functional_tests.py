"""
authentication_functional_tests.py

Thomas R Storey 6/17/16
Here we are going to break from the convention of using comments to only explain
why and use a new convention - the User Story. Writing a coherent, plain-english 
user story in comments forces us to think about the app we are writing from the
user's perspective, which naturally should be the goal of a SaaS company being
actually a people company. :)

Functional Tests:
- Test a user story
- Test that everything looks correct to a user (don't test the insides)
"""

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from pymongo import MongoClient
from microblog import bcrypt

from .bases import FunctionalBaseCase
from nose.tools import *

class AuthenticationTestCase(FunctionalBaseCase):

	def test_signup_and_login(self):
		# Joanne has heard about a new microblogging service. She goes to the 
		# homepage.
		self.browser.get(self.url)

		# She notices the page title and header mention microblogging
		self.assertIn('Welcome to microblog', self.browser.title)

		header_text = self.browser.find_element_by_id('greeting-header').text
		self.assertIn('Welcome to microblog.', header_text)

		# She is invited to sign in and start micrblogging
		signup_link = self.browser.find_element_by_id('signup_link')
		self.assertEqual(
				signup_link.get_attribute('href'),
				self.url+'/signup'
		)
		self.assertEqual(
				signup_link.text,
				'Sign Up'
		)

		# She clicks on the "sign in" button, taking her to a account creation 
		# page
		signup_link.click()

		self.assertIn('Sign Up', self.browser.title)

		# She enters her credentials and hits "sign in"
		signup_username = self.browser.find_element_by_id('username')
		signup_password = self.browser.find_element_by_id('password')
		signup_password_conf = self.browser.find_element_by_id('passwordconfirm')
		signup_submit = self.browser.find_element_by_id('submit')
		
		self.assertEqual(
			signup_username.get_attribute('placeholder'),
			'Username'
		)
		self.assertEqual(
			signup_password.get_attribute('placeholder'),
			'Password'
		)
		self.assertEqual(
			signup_password_conf.get_attribute('placeholder'),
			'Confirm Password'
		)
		self.assertEqual(
			signup_submit.get_attribute('value'),
			'Sign Up'
		)
		signup_username.send_keys('mojo_jojo')
		signup_password.send_keys('mojopassword')
		signup_password_conf.send_keys('mojopassword')
		signup_submit.click()
		
		# which takes her to the main page, on which she notes that she can now 
		# log in
		flash_signup = self.browser.find_element_by_class_name('flash-signup')
		self.assertIn(
			'Account created!',
			flash_signup.text.decode(), 
		)
		# She clicks the log in link, which takes her to the log in page
		login_link = self.browser.find_element_by_id('login_link')
		self.assertIn('Log In', login_link.text)
		login_link.click()

		# She enters her credentials and clicks "Log In"
		login_username = self.browser.find_element_by_id('username')
		login_password = self.browser.find_element_by_id('password')
		login_remember = self.browser.find_element_by_id('remember_me')
		login_submit = self.browser.find_element_by_id('submit')
		
		self.assertEqual(
			login_username.get_attribute('placeholder'),
			'Username'
		)
		self.assertEqual(
			login_password.get_attribute('placeholder'),
			'Password'
		)
		self.assertEqual(
			login_submit.get_attribute('value'),
			'Log In'
		)
		login_username.send_keys('mojo_jojo')
		login_password.send_keys('mojopassword')
		login_submit.click()

		# She is taken backto the index page, which informs her she is now
		# logged in
		flash_login = self.browser.find_element_by_class_name('flash-login')
		self.assertIn(
			'Logged in as: mojo_jojo.',
			flash_login.text.decode(), 
		)
		header_text = self.browser.find_element_by_id('greeting-header').text
		self.assertIn('Hello, mojo_jojo!', header_text)

	def test_reject_bad_signups(self):

		## Test that only signups with unique usernames and matching passwords
		## can create a user

		# Joanne hasn't had any coffee today.

		# She goes to the signup page to make a microblog account, but neglects
		# to put in a username

		self.browser.get(self.url+"/signup")

		self.signUp(None, 'mojopassword')

		# She is returned to the signup page, but realizes her mistake when she
		# sees the flash message
		self.assertFlash(
			"flash-error", 
			'Sign up error: Please input a username.'
		)

		# She tries again, but this time she forgets to put in a password
		self.signUp('mojo_jojo', None)

		# She is returned to the signup page, but realizes her mistake when she
		# sees the flash message
		self.assertFlash(
			"flash-error",
			'Sign up error: Please input a password.'
		)

		# She tries again, almost getting it, but she accidentally puts in a
		# different password in the confirm password field
		self.signUp('mojo_jojo', 'mojopassword', 'mojopasswordf')

		# She is returned to the signup page, but realizes her mistake when she
		# sees the flash message
		self.assertFlash(
			"flash-error", 
			'Sign up error: Passwords do not match.'
		)

		# Finally she gets it right, but it fails because a user already has
		# that username!
		self.insertUser('mojo_jojo', 'mojopassword')
		self.signUp('mojo_jojo', 'mojopassword')

		# She is returned to the signup page, but realizes her mistake when she
		# sees the flash message
		self.assertFlash(
			"flash-error", 
			'Sign up error: That username is already taken.'
		)

	def test_reject_bad_logins(self):

		## Test that only valid usernames with matching passwords can log in.

		# Joanne still hasn't had any coffee today.

		# She goes to the login page to access her microblog account.
		self.browser.get(self.url+"/login")

		# She tries to log in, but forgets to put in a password
		self.logIn('mojo_jojo', None)
		
		# She is returned to the login page, but realizes her mistake when she
		# sees the flash message
		self.assertFlash(
			'flash-error', 
			'Log in error: Please input a password.'
		)
		
		# She tries to log in, but forgets to put in a username
		self.logIn(None, 'mojopassword')

		# She is returned to the login page, but realizes her mistake when she
		# sees the flash message
		self.assertFlash(
			'flash-error',
			'Log in error: Please input a username.'
		)

		# She tries to log in, but mistypes her username
		self.logIn('mojp_jojo', 'mojopassword')

		# She is returned to the login page, but realizes her mistake when she
		# sees the flash message
		self.assertFlash(
			'flash-error', 
			'Log in error: Incorrect username or password.'
		)

		# She tries to log in but mistypes her password
		self.logIn('mojo_jojo', 'mojopassworf')

		# She is returned to the login page, but realizes her mistake when she
		# sees the flash message
		self.assertFlash(
			'flash-error',
			'Log in error: Incorrect username or password.'
		)

		# When she hits enter, the page updates, and her new blogpost is listed 
		# on her activity feed

		# The input box is still there.
		# She types in another update "just to make sure it still works"

		# statisfied with her blogging activity today, Jo closes the browser
		
