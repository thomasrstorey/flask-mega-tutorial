# python language imports
import os
from urlparse import urlparse
from unittest import TestCase
from datetime import datetime
# testing utility imports
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from nose.tools import *
# flask imports
from flask import render_template, url_for, current_app
# db imports
from pymongo import MongoClient
from bson.objectid import ObjectId
import gridfs
# app imports
from microblog import app, bcrypt
from microblog.models import User, Asset

class BaseCase(TestCase):

	def initDB(self):
		self.mongo = MongoClient()
		self.db = self.mongo['microblogtest']
		if('users' in self.db.collection_names()):
			self.db.drop_collection('users')
		if('posts' in self.db.collection_names()):
			self.db.drop_collection('posts')

	def initGridFS(self):

		if('assets.files' in self.db.collection_names()):
			self.db.drop_collection('assets.files')
		if('assets.chunks' in self.db.collection_names()):
			self.db.drop_collection('assets.chunks')

		self.grid_fs = gridfs.GridFS(self.db, 'assets')
		if not self.grid_fs.exists({"filename":"default_avatar.jpg"}):
			with open(
				os.path.join(app.root_path, 'static/images/default_avatar.jpg')
			) as file:
				a = Asset(
					file=file, 
					filename="default_avatar.jpg", 
					metadata={"type":"avatar"}
				)
				a.save()

	def insertUser(self, username, password):
		u = User()
		u.username = username
		u.setHashedPassword(password)
		u.save()

	def insertPost(self, username, text):
		p = Post()
		u = User(username)
		p.text = text
		p.user_id = u.id
		p.save()

	# def clearUserPosts(self, username):
	# 	if "posts" in self.db.collection_names():
	# 		u = User(username=username)
	# 		ps = Posts(user_id=ObjectId(u.id))
	# 		ps.remove()


class UnitBaseCase(BaseCase):

	def setUp(self):
		app.config.from_object('config.TestingConfig')
		self.client = app.test_client(self)

	def tearDown(self):
		pass

	def assertFlashes(self, expected_message, expected_category='message'):
		with self.client.session_transaction() as session:
			try:
				category, message = session['_flashes'][0]
			except KeyError:
				raise AssertionError('nothing flashed')

			self.assertIn(expected_message, message)
			self.assertEqual(expected_category, category)

	def assertUser(self, expected_username='anonymous'):
		with self.client.session_transaction() as session:
			try:
				user_id = session['user_id']
			except KeyError:
				if expected_username == 'anonymous':
					return self.assertTrue(session.get('user_id') == None)
				else :
					raise AssertionError('not logged in')

			user = self.db.users.find_one({"_id":ObjectId(user_id)})
			
			if expected_username == 'anonymous':
				self.assertTrue(user == None)

			else :
				self.assertTrue(user != None)
				self.assertEqual(user["username"], expected_username)

	def signUp(self, username, password, confirm=None, follow_redirects=False):
		if confirm == None:
			confirm = password
		return self.client.post(
				path='/signup', 
				data=dict(
					username=username,
					password=password,
					password_confirm=confirm,
					submit="Sign Up"		
				), follow_redirects=follow_redirects)

	def logIn(self, username, password, follow_redirects=False):
		return self.client.post(
				path='/login', 
				data=dict(
					username=username,
					password=password,
					submit="Log In"		
				), follow_redirects=follow_redirects)

	def clearFlashes(self):
		with self.client.session_transaction() as session:
			session['_flashes'] = []


class FunctionalBaseCase(BaseCase):
	def setUp(self):
		self.initDB()
		self.initGridFS()
		self.browser = webdriver.Chrome()
		self.browser.implicitly_wait(3)
		self.url = "http://microblog.local:5000"

	def tearDown(self):
		self.browser.quit()

	def signUp(self, un=None, pw=None, conf=None):
		if conf == None and pw != None:
			conf = pw

		username = self.browser.find_element_by_id('username')
		password = self.browser.find_element_by_id('password')
		password_conf = self.browser.find_element_by_id('passwordconfirm')
		submit = self.browser.find_element_by_id('submit')
		
		if un != None:
			username.send_keys(un)
		if pw != None:
			password.send_keys(pw)
		if conf != None:
			password_conf.send_keys(conf)

		submit.click()

	def logIn(self, un=None, pw=None, rm=None):
		username = self.browser.find_element_by_id('username')
		password = self.browser.find_element_by_id('password')
		remember_me = self.browser.find_element_by_id('remember_me')
		submit = self.browser.find_element_by_id('submit')
		
		if un != None:
			username.send_keys(un)
		if pw != None:
			password.send_keys(pw)
		if rm != None:
			remember_me.click()

		submit.click()

	def assertFlash(self, flash_class, flash_message):
		flash = self.browser.find_element_by_class_name(flash_class)
		self.assertIn(
			flash_message,
			flash.text.decode(), 
		)

	def assertElementExists(self, by=By.ID, value=None):
		try:
			el = self.browser.find_element(by, value)
		except NoSuchElementException:
			self.fail(
				"Element '%s' identified by '%s' is not linked to the DOM."
				% (value, by)
				)
		else:
			return el

	def assertValidDatetime(self, dt_string, dt_format):
		try:
			datetime.strptime(dt_string, dt_format)
		except ValueError:
			self.fail(
				"String '%s' does not match format '%s'." 
				% dt_string, dt_format
			)
