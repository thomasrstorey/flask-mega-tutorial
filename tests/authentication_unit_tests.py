"""
authentication_unit_tests.py

Unit tests: 
- Test one thing in each test
- Test all the things that need to work for the functional test to pass

Refactoring:
- The idea of refactoring makes no sense without tests. Without tests, how do 
  know that you didn't break it in the course of changing the insides?

Triangulation:
- Writing more specific tests to force refactoring to a more robust
  implementation of a feature, from a simply passing one.

"""

import os
from .bases import UnitBaseCase
from nose.tools import *
from bson.objectid import ObjectId

from flask import render_template, url_for, current_app
from pymongo import MongoClient
from urlparse import urlparse

from microblog import app

class TestSignUp(UnitBaseCase):
	
	def setUp(self):
		self.initDB()

		super(TestSignUp, self).setUp()

		self.clearFlashes()

	def tearDown(self):
		pass

	def test_signup_page_can_create_user(self):
		res = self.signUp('unit_test', 'utpass')
		# we want to test the db directly as well as the response to make
		# sure everything is in order front and back.
		# test db directly
		self.assertTrue('users' in self.db.collection_names())

		users = self.db['users']
		test_user = users.find_one({'username':'unit_test'})

		self.assertEqual(users.count(), 1)
		self.assertTrue(test_user != None)
		self.assertEqual(test_user['username'], 'unit_test')
		

	def test_signup_page_redirects_after_create_user(self):
		res = self.signUp('unit_test', 'utpass')
		
		self.assertEqual(res.status_code, 302)
		with app.app_context():
			self.assertEqual(
				res.headers.get('Location'),
				url_for('index')
			)
		

	def test_signup_page_flashes_after_create_user(self):
		res = self.signUp('unit_test', 'utpass')

		self.assertFlashes(
			'Account created!',
			'flash-signup'
		)

	def test_signup_page_flashes_after_reject_empty_password(self):
		res = self.signUp('unit_test', '')

		self.assertFlashes(
			'Sign up error',
			'flash-error'
		)

	def test_signup_page_flashes_after_reject_empty_username(self):
		res = self.signUp('', 'utpass')

		self.assertFlashes(
			'Sign up error',
			'flash-error'
		)

	def test_signup_page_flashes_after_reject_nonmatching_passwords(self):
		res = self.signUp('unit_test', 'utpass', 'itpass')

		self.assertFlashes(
			'Sign up error',
			'flash-error'
		)

	def test_signup_page_flashes_after_reject_extant_username(self):
		self.signUp('unit_test', 'utpass')
		self.clearFlashes()
		self.signUp('unit_test', 'utpass')

		self.assertFlashes(
			'Sign up error',
			'flash-error'
		)

	def test_signup_page_redirects_after_reject(self):
		res = self.signUp('', 'utpass')

		self.assertEqual(res.status_code, 302)
		with app.app_context():
			self.assertEqual(
				res.headers.get('Location'),
				url_for('signup')
			)
		

	def test_signup_page_rejects_empty_username(self):
		res = self.signUp('', 'utpass')

		self.assertFalse('users' in self.db.collection_names())

	def test_signup_page_rejects_empty_password(self):
		res = self.signUp('unit_test', '')

		self.assertFalse('users' in self.db.collection_names())

	def test_signup_page_rejects_extant_username(self):
		self.signUp('unit_test', 'utpass1')
		self.clearFlashes()
		self.signUp('unit_test', 'utpass1')
		
		users = self.db['users']
		
		self.assertEqual(users.count(), 1)


class TestLogIn(UnitBaseCase):

	def setUp(self):
		self.initDB()

		super(TestLogIn, self).setUp()

		self.signUp('unit_test', 'utpass')
		self.clearFlashes()

	def tearDown(self):
		pass

	def test_login_page_can_login_user(self):
		res = self.logIn('unit_test', 'utpass', True)

		self.assertUser('unit_test')

	def test_login_page_redirects_after_login(self):
		res = self.logIn('unit_test', 'utpass')

		self.assertEqual(res.status_code, 302)
		with app.app_context():
			self.assertEqual(
				res.headers.get('Location'), 
				url_for('index')
			)

	def test_login_page_flashes_after_login(self):
		res = self.logIn('unit_test', 'utpass')

		self.assertFlashes(
			'Logged in as: unit_test',
			'flash-login'
		)

	def test_login_page_flashes_after_reject_empty_username(self):
		res = self.logIn('', 'utpass')

		self.assertFlashes(
			'Log in error',
			'flash-error'
		)

	def test_login_page_flashes_after_reject_empty_password(self):
		res = self.logIn('unit_test', '')

		self.assertFlashes(
			'Log in error',
			'flash-error'
		)

	def test_login_page_flashes_after_reject_nonextant_username(self):
		res = self.logIn('ynit_test', 'utpass')

		self.assertFlashes(
			'Log in error',
			'flash-error'
		)

	def test_login_page_flashes_after_reject_nonmatching_password(self):
		res = self.logIn('unit_test', 'utpasd')

		self.assertFlashes(
			'Log in error',
			'flash-error'
		)

	def test_login_page_redirects_after_reject(self):
		res = self.logIn('', '')

		self.assertEqual(res.status_code, 302)
		with app.app_context():
			self.assertEqual(
				res.headers.get('Location'), 
				url_for('login')
			)

	def test_login_page_rejects_empty_username(self):
		res = self.logIn('', 'utpass')
		
		self.assertUser('anonymous')

	def test_login_page_rejects_empty_password(self):
		res = self.logIn('unit_test', '')
		
		self.assertUser('anonymous')

	def test_login_page_rejects_nonextant_username(self):
		res = self.logIn('bargle', 'utpass')
		
		self.assertUser('anonymous')

	def test_login_page_rejects_nonmatching_password(self):
		res = self.logIn('unit_test', 'bargle')

		self.assertUser('anonymous')