import os
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from pymongo import MongoClient
from microblog import bcrypt

from .bases import FunctionalBaseCase
from nose.tools import *

class UserTestCase(FunctionalBaseCase):
	"""
	TestCase for the user route.
	"""
	def setUp(self):
		super(UserTestCase, self).setUp()
		self.username = "mojo_jojo"
		self.password = "mojopass"
		self.insertUser(self.username, self.password)

	def test_user_can_view_her_page(self):
		# Joanne logs in to her account
		self.browser.get(self.url+"/login")
		self.logIn(self.username, self.password)

		# She clicks on her username to go to her user page
		user_link = self.assertElementExists(By.ID, "user_link")
		self.assertEqual(user_link.text.decode(), "My Profile")
		user_link.click()

		# Here she can see her avatar, a button to upload a new avatar, her 
		# posts, and a field to type in a new post.
		avatar_img = self.assertElementExists(
			By.ID, 
			"avatar_top_img"
		)
		avatar_edit_file = self.assertElementExists(
			By.ID, 
			"avatar_edit_file"
		)
		avatar_edit_submit = self.assertElementExists(
			By.ID, 
			"avatar_edit_submit"
		)
		posts_container = self.assertElementExists(
			By.ID,
			"posts_container"
		)
		post_field = self.assertElementExists(By.ID, "post_field")
		post_submit = self.assertElementExists(By.ID, "post_submit")
		# Check attributes
		self.assertEqual(avatar_img.get_attribute("alt"), "default_avatar.jpg")
		self.assertEqual(
			avatar_edit_file.get_attribute("accept"),
			"image/jpg,image/png,image/jpeg,image/gif"
		)
		self.assertEqual(
			avatar_edit_submit.get_attribute("value"),
			"Upload"
		)
		self.assertEqual(
			posts_container.find_element_by_id("no_posts_text").text.decode(),
			"No posts yet!"
		)
		self.assertEqual(
			post_field.get_attribute("placeholder"),
			"What's up?"
		)
		self.assertEqual(
			post_submit.get_attribute("value"),
			"Submit Post"
		)

		# she can also see her bio and her last login date/time
		bio = self.assertElementExists(By.ID, "user_bio")
		bio_edit = self.assertElementExists(By.ID, "bio_edit_text")
		last_login = self.assertElementExists(By.ID, "last_login")

		self.assertEqual(bio.text.decode(), "Who are you?")
		self.assertEqual(bio_edit.get_attribute("value"), "Who are you?")
		# self.assertValidDatetime(last_login.text.decode(), "%m/%d/%y %I:%M")


	def test_user_can_upload_and_view_her_avatar(self):
		# Joanne logs in and goes to her user page.
		self.browser.get(self.url+"/login")
		self.logIn(self.username, self.password)
		self.browser.get(self.url+"/"+self.username)

		# she decides its time to freshen up her online look so she finds the
		# upload avatar form fields
		upload_file = self.assertElementExists(By.ID, "avatar_edit_file")
		upload_submit = self.assertElementExists(By.ID, "avatar_edit_submit")

		# she picks a file from her computer and uploads it
		upload_file.send_keys(os.getcwd()+"/tests/test_avatar.jpg")
		upload_submit.click()

		# the page reloads and now she is back at her user page, and she can 
		# see that her avatar is updated.
		avatar_img = self.assertElementExists(
			By.ID, 
			"avatar_top_img"
		)
		self.assertEqual(avatar_img.get_attribute("alt"), "test_avatar.jpg")


	def test_user_can_post_and_view_her_posts(self):
		# Joanne logs in and goes to her user page.
		self.browser.get(self.url+"/login")
		self.logIn(self.username, self.password)
		self.browser.get(self.url+"/"+self.username)

		# she has a lot on her mind right now, so she decides to vent in a 
		# new microblog post
		post_field = self.assertElementExists(By.ID, "post_field")
		post_submit = self.assertElementExists(By.ID, "post_submit")

		text = "My boss was so mean today. I wish I worked at Feathr! :("

		post_field.send_keys(text)
		post_submit.click()

		# the page reloads and she sees her post in the list
		posts = self.browser.find_elements_by_class_name("post_text")
		self.assertEqual(len(posts), 1)
		self.assertEqual(posts[0].text.decode(), text)

	def test_user_can_post_multiple_times(self):
		self.fail("Finish the test!")

	def test_user_can_update_and_view_her_bio(self):
		self.fail("Finish the test!")
