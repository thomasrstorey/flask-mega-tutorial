from .bases import UnitBaseCase

class TestIndex(UnitBaseCase):

	def test_root_url_resolves_to_index(self):
		res = self.client.get('/', content_type='html/text')
		self.assertEqual(res.status_code, 200)
		self.assertTrue(res.get_data().startswith('<!DOCTYPE html>\n<html>'))
		self.assertIn('<title>', res.get_data())
		self.assertTrue(res.get_data().endswith('</html>'))

	def test_index_url_resolves_to_index(self):
		res = self.client.get('/index', content_type='html/text')
		self.assertEqual(res.status_code, 200)
		self.assertTrue(res.get_data().startswith('<!DOCTYPE html>\n<html>'))
		self.assertIn('<title>', res.get_data())
		self.assertTrue(res.get_data().endswith('</html>'))	