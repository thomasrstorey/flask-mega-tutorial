#Flask Mega Tutorial
Thomas R Storey
###Notes
From TDD with Python Chapter 3:
"Ultimately, programming is hard. Often, we are smart, so we succeed. TDD is there to help us out when we’re not so smart. Kent Beck (who basically invented TDD) uses the metaphor of lifting a bucket of water out of a well with a rope: when the well isn’t too deep, and the bucket isn’t very full, it’s easy. And even lifting a full bucket is pretty easy at first. But after a while, you’re going to get tired. TDD is like having a ratchet that lets you save your progress, take a break, and make sure you never slip backwards. That way you don’t have to be smart all the time."

Functional Tests:
 * Test a user story
 * Test that everything looks correct to a user (don't test the insides)

Unit tests: 
 * Test one thing in each test
 * Test all the things that need to work for the functional test to pass

Refactoring:
 * The idea of refactoring makes no sense without tests. Without tests, how do you know that you didn't break it in the course of changing the insides?

Triangulation:
 * Writing more specific tests to force refactoring to a more robust implementation of a feature, from a simply passing one.