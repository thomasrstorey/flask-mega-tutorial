try:
	from setuptools import setup
except ImportError:
	from distutils.core import setup

config = {
	'description'      : 'Flask Mega Tutorial Microblog',
	'author'           : 'Thomas R Storey',
	'url'              : '',
	'download_url'     : '',
	'author_email'     : 'thomas@feathr.co',
	'version'          : '0.0.1',
	'install_requires' : ['nose', 'flask', 'selenium'],
	'packages'         : ['microblog'],
	'scripts'          : [],
	'name'             : 'flask-mega-tutorial-thomas'
}

setup(**config)