import os
from flask import Flask
from pymongo import MongoClient
import gridfs
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
# create app variable from Flask used by app package's views module
app = Flask(__name__)
# this gets the config info from config.py
# app.config.from_object('config.ProductionConfig')
app.config.from_object('config.TestingConfig')

mongo = MongoClient()
db = mongo[app.config["MONGO_DBNAME"]]
grid_fs = gridfs.GridFS(db, 'assets')
bcrypt = Bcrypt(app)

lm = LoginManager()
lm.init_app(app)
lm.login_view = 'login'

from microblog import views
from microblog.models import Asset
# install database
if not grid_fs.exists({"filename":"default_avatar.jpg"}):
	with open(
		os.path.join(app.root_path, 'static/images/default_avatar.jpg')
		) as file:
		a = Asset(
			file=file, 
			filename="default_avatar.jpg", 
			metadata={"type":"avatar"}
		)
		a.save()
		# views.upload_asset(file, "default_avatar.jpg", {"type":"avatar"})
