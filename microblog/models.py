import mimetypes
from bson.objectid import ObjectId
from microblog import db, grid_fs, bcrypt
from datetime import datetime

class User(object):
	"""
	User class fulfilling the interface described in the Flask-Login
	documentation at https://flask-login.readthedocs.io/en/latest/
	"""

	id 			= None
	username 	= None
	password 	= None
	avatar_id 	= None
	bio 		= None
	last_login 	= None
	password 	= None

	existing	= None


	def __init__(self, id=None, username=None):
		# get a user by id or username
		# return new User object if no identifiers provided or user not found.
		user_doc = None
		if id != None:
			user_doc = db.users.find_one({"_id":ObjectId(id)})

		elif username != None:
			user_doc = db.users.find_one({"username":username})

		if user_doc != None:
			# found a user
			self.existing = True
			self.id = user_doc["_id"]
			self.username = user_doc["username"]
			self.password = user_doc["password"] #hashed password
			self.avatar_id = user_doc["avatar_id"]
			self.bio = user_doc["bio"]
			self.last_login = user_doc["last_login"]

		else :
			# make new user
			self.existing = False
			assets = db.assets.files
			self.avatar_id = ObjectId(assets.find_one(
				{"filename":"default_avatar.jpg"}
			)["_id"])
			self.bio = "Who are you?"
			self.last_login = datetime.utcnow()

	def setHashedPassword(self, password):
		self.password = bcrypt.generate_password_hash(password)
				

	def save(self):
		if self.existing:
			return self.update()
		if self.username == None or self.password == None:
			raise ValueError("User needs username and password before saving.")

		return db.users.insert_one({
			"username"	 : self.username,
			"password"	 : self.password,
			"avatar_id"  : ObjectId(self.avatar_id),
			"bio"		 : self.bio,
			"last_login" : self.last_login
		})

	def update(self):
		# get current user dict
		old = db.users.find_one({"_id":ObjectId(self.id)})
		# get this object properties as a dict
		new = dict(
			username=self.username,
			password=self.password,
			avatar_id=ObjectId(self.avatar_id),
			bio=self.bio,
			last_login=self.last_login
		)
		# get dict with only the keys which are different
		_set = {}
		for key in new.keys():
			oldval = old.get(key)
			if new[key] != oldval:
				_set[key] = new[key]
		# $set properties on user
		db.users.update({"_id":ObjectId(self.id)},
			{"$set":_set})

	def getPosts(self):
		posts = db.posts.find({"user_id":ObjectId(self.id)})
		return posts


	@property	
	def is_authenticated(self):
		return True

	@property
	def is_active(self):
		return True

	@property
	def is_anonymous(self):
		return False

	def get_id(self):
		return unicode(self.id)




class Asset(object):
	"""docstring for Asset"""

	asset_object = None
	id 			 = None
	filename     = None
	asset_type   = None
	content_type = None
	_file_obj	 = None

	def __init__(self, filename=None, file=None, metadata=None):
		if file == None and filename != None:
			# must be querying if no file provided
			if not grid_fs.exists({"filename":filename}):
				raise ModelError("That asset doesn't exist.")

			asset_doc = db.assets.files.find_one({"filename":filename})
			self.id = asset_doc["_id"]
			self.filename = filename
			self.asset_type = asset_doc["metadata"]["type"]
			self.content_type = asset_doc["contentType"]

			# GridOut Object (readable file)
			self.asset_object = grid_fs.get_last_version(filename=filename)

		elif file != None and filename != None:
			# if file provided, ready it for upload
			self.filename = filename
			if metadata != None:
				self.asset_type = metadata["type"]
			self._file_obj = file

		else:
			raise ModelError(
				"Asset requires at least a filename as a keyword argument."
			)

	def read(self):
		return self.asset_object.read()

	def save(self):
		"""
		see:
		https://docs.mongodb.com/ecosystem/use-cases/
		metadata-and-asset-management/#upload-a-photo
		"""
		default_meta = dict(
	        locked=datetime.utcnow()
		)
		mime_type = mimetypes.guess_type(self.filename)[0]
		# merge custom meta dict into default meta dict
		metadata = default_meta.copy()
		metadata.update({"type":self.asset_type})

		with grid_fs.new_file(
	    	content_type=mime_type, 
	    	filename=self.filename,
	        metadata=metadata
	    ) as upload_file:
			while True:
				# read from the file, write to the db
				chunk = self._file_obj.read(upload_file.chunk_size)
				if not chunk: break
				upload_file.write(chunk)
			# unlock the file
			db.assets.files.update(
			    {'_id': upload_file._id},
			    {'$set': { 'metadata.locked': None } } )
			self.id = upload_file._id
	
	
		

class Post(object):

	id 		= None
	text 	= None
	user_id = None
	
	def __init__(self, id=None):
		if id != None:
			post_doc = db.posts.find_one({"_id":ObjectId(id)})
			if post_doc:
				self.id = post_doc["_id"]
				self.text = post_doc["text"]
				self.user_id = post_doc["user_id"]
			else:
				raise ModelError("Post not found in the database.")

	def save():
		db.posts.insert_one({
			"text":self.text,
			"user_id":ObjectId(self.user_id)
		})

	def update():
		# get current user dict
		old = db.posts.find_one({"_id":ObjectId(self.id)})
		# get this object properties as a dict
		new = dict(
			text=self.text,
			user_id=ObjectId(self.user_id),
		)
		# get dict with only the keys which are different
		_set = {}
		for key in new.keys():
			oldval = old.get(key)
			if new[key] != oldval:
				_set[key] = new[key]
		# $set properties on user
		db.posts.update({"_id":ObjectId(self.id)},
			{"$set":_set})


class ModelError(Exception):
	pass
			