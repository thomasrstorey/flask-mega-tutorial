from datetime import datetime
from flask import (render_template, redirect, flash, request, session, url_for, 
	g, send_file, abort, make_response)
from werkzeug.utils import secure_filename
from flask_login import login_user, logout_user, current_user, login_required
from microblog import app, db, grid_fs, bcrypt, lm, forms
from bson.objectid import ObjectId
from .models import User, Asset, Post, ModelError

@lm.user_loader
def load_user(id):
	u = User(id=id)
	if u.id:
		return u
	else:
		return None

@app.before_request
def before_request():
	g.user = current_user

@app.route('/')
@app.route('/index')
def index():
	return render_template('index.html', user=g.user)

@app.route('/signup', methods=['GET', 'POST'])
def signup():
	form = forms.SignUpForm(csrf_enabled=app.config['WTF_CSRF_ENABLED'])
	if request.method == "POST":
		if form.validate_on_submit():
			# determine if username already exists
			extant_user = User(username=form.username.data)
			if extant_user.username != None:
				flash(
					"Sign up error: That username is already taken.",
					"flash-error"
				)
				return redirect(url_for('signup'))
			else:
				# create user
				flash(
					"Account created! "
					"You can now log in with your username and password.",
					'flash-signup'
				)
				user = User()
				user.username = form.username.data
				user.setHashedPassword(form.password.data)
				user.save()
				return redirect(url_for('index'))

		else :
			for field, messages in form.errors.iteritems():
				for msg in messages:
					flash(msg, 'flash-error')
			return redirect(url_for('signup'))

		
	return render_template(
		'signup.html',
		title="Sign Up",
		form=form,
		user=g.user
	)

@app.route('/login', methods=['GET', 'POST'])
def login():
	form = forms.LogInForm(csrf_enabled=app.config['WTF_CSRF_ENABLED'])
	if request.method == "POST":
		if form.validate_on_submit():
			# test if user exists
			user = User(username=form.username.data)

			if (
			    user.existing and
			    bcrypt.check_password_hash(user.password, form.password.data)
			   ):
				# log in user
				login_user(user)
				session['remember_me'] = form.remember_me.data
				flash('Logged in as: %s.' % user.username, 'flash-login')
				return redirect(url_for('index'))
			else :
				# username or password don't match
				flash(
					'Log in error: Incorrect username or password.',
					'flash-error'
				)
				return redirect(url_for('login'))
		else :
			for field, messages in form.errors.iteritems():
				for msg in messages:
					flash(msg, 'flash-error')
			return redirect(url_for('login'))

	return render_template(
		'login.html',
		title="Log In",
		form=form,
		user=g.user
	)

@app.route('/logout')
@login_required
def logout():
	logout_user()
	return redirect(url_for('index'))

@app.route('/<username>', methods=['GET', 'POST'])
def user(username):
	assets = db.assets.files
	users = db.users
	page_user = User(username=username)

	if not page_user.existing:
		abort(404)

	try:
		if g.user.username == page_user.username:
			# make forms
			avatar_edit = forms.AvatarEditForm(
				csrf_enabled=app.config['WTF_CSRF_ENABLED']
			)
			new_post = forms.NewPostForm(
				csrf_enabled=app.config['WTF_CSRF_ENABLED']
			)
			bio_edit = forms.BioEditForm(
				data=dict(bio_edit_text=page_user.bio),
				csrf_enabled=app.config['WTF_CSRF_ENABLED']
			)
			user_forms = dict(
				avatar_edit=avatar_edit,
				new_post=new_post,
				bio_edit=bio_edit
			)
		else:
			user_forms = None

	except AttributeError:
		# anonymous user - same difference
		user_forms = None

	avatar = assets.find_one({"_id":ObjectId(page_user.avatar_id)})
	bio = page_user.bio
	last_login = page_user.last_login

	posts = page_user.getPosts()
	if not posts.count():
		posts = None
	return render_template(
		'user.html',
		title=username,
		user=g.user,
		avatar=dict(
			filename=avatar["filename"],
			url=url_for('asset', filename=avatar['filename'])
		),
		forms=user_forms,
		bio=bio,
		last_login=last_login,
		posts=posts
	)

@app.route('/assets/<filename>', methods=['GET'])
def asset(filename):
	try:
		a = Asset(filename=filename)
		res = make_response(a.read())
		res.mimetype = a.content_type
		return res
	except ModelError:
		return abort(404)

@app.route('/assets/new', methods=['POST'])
@login_required
def new_asset():
	# get asset type if it exists
	if request.form.get("asset_type") == "avatar":
		# validate form data
		form = forms.AvatarEditForm(csrf_enabled=app.config['WTF_CSRF_ENABLED'])
		if form.validate():

			file = request.files["avatar_edit_file"]

			filename = secure_filename(file.filename)
			# upload asset and save
			a = Asset(
				file=file, 
				filename=filename, 
				metadata=dict(type="avatar")
			)
			a.save()

			# now update the user avatar_id
			u = User(username=form.avatar_user.data)
			u.avatar_id = a.id
			u.update()
				
			return redirect(url_for('user', username=form.avatar_user.data))
		
		else :
			for field, messages in form.errors.iteritems():
				for msg in messages:
					flash(msg, 'flash-error')
			return redirect(url_for('user', username=form.avatar_user.data))

	else:
		return abort(404)

