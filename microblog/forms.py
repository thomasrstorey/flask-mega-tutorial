from flask_wtf import Form
from wtforms import (StringField, PasswordField, SubmitField, BooleanField,
	 TextAreaField, HiddenField)
from wtforms.validators import DataRequired, EqualTo, Regexp
from flask_wtf.file import FileField
from flask_wtf.file import file_required, file_allowed

class UploadSet(object):
    def __init__(self, name='files', extensions=None):
        self.name = name
        self.extensions = extensions

    def file_allowed(self, storage, basename):
        if not self.extensions:
            return True

        ext = basename.rsplit('.', 1)[-1]
        return ext in self.extensions

images = UploadSet('images', ['jpg', 'jpeg', 'gif', 'png'])

class LogInForm(Form):
	username = StringField(
		label='Username', 
		validators=[DataRequired("Log in error: Please input a username.")]
	)
	password = PasswordField(
		label='Password',
		validators=[DataRequired("Log in error: Please input a password.")]
	)
	remember_me = BooleanField(
		label='Remember Me', 
		default=False
	)
	submit = SubmitField(
		label='Submit'
	)


class SignUpForm(Form):
	username = StringField(
		label='Username',
		validators=[DataRequired("Sign up error: Please input a username.")]
	)
	password = PasswordField(
		label='Password', 
		validators=[DataRequired("Sign up error: Please input a password.")]
	)
	password_confirm = PasswordField(
		label='Confirm Password', 
		validators=[
			DataRequired("Sign up error: Passwords do not match."), 
			EqualTo('password', "Sign up error: Passwords do not match.")
		],
		id='passwordconfirm'
	)
	submit = SubmitField(
		label='Submit'
	)

class AvatarEditForm(Form):
	asset_type = HiddenField()
	avatar_user = HiddenField()
	avatar_edit_file = FileField(
		validators=[file_required(),
        file_allowed(images)]
	)
	avatar_edit_submit = SubmitField(
		label='Upload Avatar'
	)

class NewPostForm(Form):
	post_field = TextAreaField(
		label='New Post',
		validators=[DataRequired("Write a post!")]
	)
	post_submit = SubmitField(
		label='Submit'
	)

class BioEditForm(Form):
	bio_edit_text = TextAreaField(
		label='Edit Bio',
		validators=[DataRequired("Tell us who you are!")]
	)
	bio_edit_submit = SubmitField(
		label='Edit Bio'
	)