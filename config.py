class Config(object):
	"""
	Following the inheritance model of configuration, described in the
	flask docs at: 
	http://flask.pocoo.org/docs/0.11/config/#development-production
	"""
	DEBUG = False
	TESTING = False

	SECRET_KEY = "1@Re$q3QVF5SE^RTbjDzAYvaAwZH6K1n"


class ProductionConfig(Config):

	WTF_CSRF_ENABLED = True

	MONGO_DBNAME = 'microblog'
	

class TestingConfig(Config):
	
	TESTING = True

	WTF_CSRF_ENABLED = False

	MONGO_DBNAME = 'microblogtest'

	SERVER_NAME = 'microblog.local:5000'
	
		

